object Form1: TForm1
  Left = 0
  Top = 0
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
  ClientHeight = 296
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListBoxMaterials: TListBox
    Left = 8
    Top = 8
    Width = 185
    Height = 241
    ItemHeight = 13
    TabOrder = 0
    OnClick = ListBoxMaterialsClick
    OnKeyDown = ListBoxMaterialsKeyDown
  end
  object ButtonAdd: TButton
    Left = 8
    Top = 263
    Width = 81
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 1
    OnClick = ButtonAddClick
  end
  object ButtonDelete: TButton
    Left = 95
    Top = 263
    Width = 81
    Height = 25
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 2
    OnClick = ButtonDeleteClick
  end
  object ButtonEdit: TButton
    Left = 182
    Top = 263
    Width = 99
    Height = 25
    Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
    TabOrder = 3
    OnClick = ButtonEditClick
  end
  object ButtonClose: TButton
    Left = 312
    Top = 263
    Width = 69
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 4
    OnClick = ButtonCloseClick
  end
  object Memo1: TMemo
    Left = 200
    Top = 8
    Width = 177
    Height = 241
    Lines.Strings = (
      'Memo1')
    TabOrder = 5
  end
end
