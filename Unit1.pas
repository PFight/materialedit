unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Generics.Collections, ShellAPI, Model, Unit2;


type
  TForm1 = class(TForm)
    ListBoxMaterials: TListBox;
    ButtonAdd: TButton;
    ButtonDelete: TButton;
    ButtonEdit: TButton;
    ButtonClose: TButton;
    Memo1: TMemo;
    procedure ButtonCloseClick(Sender: TObject);
    procedure ListBoxMaterialsClick(Sender: TObject);
    procedure ListBoxMaterialsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButtonAddClick(Sender: TObject);
    procedure ButtonEditClick(Sender: TObject);
    procedure ButtonDeleteClick(Sender: TObject);
    procedure UpdateList();
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.ButtonAddClick(Sender: TObject);
var material: TMaterial;
    formResult: Integer;
    paramFile, resultFile: TextFile;
begin
   Cursor := crHourGlass;

   material:= TMaterial.Create();
   material.name := 'Material';
   Form2.setMaterial(material);
   Form2.ShowModal();
   if (Form2.mModalResult = mrOk)  then begin

       AssignFile(paramFile, 'input.dat');
       ReWrite(paramFile);
       WriteLn(paramFile, 'Create');
       WriteLn(paramFile, 'Material');
       WriteLn(paramFile, 'name=' +  material.name);
       WriteLn(paramFile, 'cost=' + FloatToStr(material.cost));
       WriteLn(paramFile, 'density=' + FloatToStr(material.density));
       WriteLn(paramFile, 'fire_resist=' + FloatToStr(material.fire_resist));
       WriteLn(paramFile, 'stability=' + FloatToStr(material.stability));

       CloseFile(paramFile);

       ShellExecuteAndWait('ConnectionUtility.exe');

       UpdateList();

       {AssignFile(resultFile, 'output.dat');
       Reset(resultFile);

       Readln(resultFile, countOfFields);
       CloseFile(resultFile);}
    end;
    Cursor := crDefault;
end;

procedure TForm1.ButtonCloseClick(Sender: TObject);
begin
    Close();
end;

procedure TForm1.ButtonDeleteClick(Sender: TObject);
var material: TMaterial;
    paramFile, resultFile: TextFile;
begin
    Cursor := crHourGlass;

    if (ListBoxMaterials.ItemIndex >= 0) then begin
       material := ListBoxMaterials.Items.Objects[ListBoxMaterials.ItemIndex] as TMaterial;
       ListBoxMaterials.Items.Delete(ListBoxMaterials.ItemIndex);
       ListBoxMaterialsClick(nil);

       AssignFile(paramFile, 'input.dat');
       ReWrite(paramFile);
       WriteLn(paramFile, 'delete');
       WriteLn(paramFile, 'material');
       WriteLn(paramFile, 'id=', material.id);
       CloseFile(paramFile);

       ShellExecuteAndWait('ConnectionUtility.exe');

       {AssignFile(resultFile, 'output.dat');
       Reset(resultFile);
       Readln(resultFile, countOfFields);
       CloseFile(resultFile);}
    end;
    Cursor := crDefault;
end;

procedure TForm1.ButtonEditClick(Sender: TObject);
var material: TMaterial;
    formResult: Integer;
    paramFile, resultFile: TextFile;
begin
    Cursor := crHourGlass;

    if (ListBoxMaterials.ItemIndex >= 0) then begin
        material := ListBoxMaterials.Items.Objects[ListBoxMaterials.ItemIndex] as TMaterial;
        Form2.setMaterial(material);
        Form2.ShowModal();
        ListBoxMaterials.Items[ListBoxMaterials.ItemIndex] := material.name;
        ListBoxMaterialsClick(nil);

        if (Form2.mModalResult = mrOk)  then begin
           AssignFile(paramFile, 'input.dat');
           ReWrite(paramFile);
           WriteLn(paramFile, 'update');
           WriteLn(paramFile, 'material');
           WriteLn(paramFile, 'id=', material.id);
           WriteLn(paramFile, 'name=' +  material.name);
           WriteLn(paramFile, 'cost=' + FloatToStr(material.cost));
           WriteLn(paramFile, 'density=' + FloatToStr(material.density));
           WriteLn(paramFile, 'fire_resist=' + FloatToStr(material.fire_resist));
           WriteLn(paramFile, 'stability=' + FloatToStr(material.stability));

           CloseFile(paramFile);

           ShellExecuteAndWait('ConnectionUtility.exe');

           {AssignFile(resultFile, 'output.dat');
           Reset(resultFile);

           Readln(resultFile, countOfFields);
           CloseFile(resultFile);}
        end;
    end;
    Cursor := crDefault;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    UpdateList();
    Memo1.Text := '';
end;

procedure TForm1.UpdateList();
var paramFile, resultFile: TextFile;
    countOfFields, countOfAssociativeModels: Integer;
    material : TMaterial;
    mass: Real;
begin

   Cursor := crHourGlass;

   AssignFile(paramFile, 'input.dat');
   ReWrite(paramFile);
   WriteLn(paramFile, 'FindAll');
   WriteLn(paramFile, 'Material');
   CloseFile(paramFile);

   ShellExecuteAndWait('ConnectionUtility.exe');

   AssignFile(resultFile, 'output.dat');
   Reset(resultFile);
   ListBoxMaterials.Items.Clear();
   while not EOF(resultFile) do begin
       material:= TMaterial.Create();

       Readln(resultFile, countOfFields);

       Readln(resultFile, material.id);
       Readln(resultFile, material.name);
       Readln(resultFile, material.cost);
       Readln(resultFile, material.density);
       Readln(resultFile, material.fire_resist);
       Readln(resultFile, material.stability);
       Readln(resultFile, mass);// ?
       Readln(resultFile, countOfAssociativeModels);

       ListBoxMaterials.AddItem(material.name, material);
   end;

   CloseFile(resultFile);

   Cursor := crDefault;
end;

procedure TForm1.ListBoxMaterialsClick(Sender: TObject);
var m: TMaterial;
begin
   if (ListBoxMaterials.ItemIndex >= 0) then begin
      m := ListBoxMaterials.Items.Objects[ListBoxMaterials.ItemIndex] as TMaterial;
      Memo1.Text := '�������� ���������: ' + sLineBreak +
        '��������: ' + m.name + sLineBreak  +
        '����: ' + FloatToStr(m.cost)  + sLineBreak  +
        '���������: ' + FloatToStr(m.density)  + sLineBreak  +
        '�������������: ' + FloatToStr(m.fire_resist)  + sLineBreak  +
        '���������: ' + FloatToStr(m.stability);
   end;
end;

procedure TForm1.ListBoxMaterialsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   ListBoxMaterialsClick(nil);
end;

end.

