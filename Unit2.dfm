object Form2: TForm2
  Left = 0
  Top = 0
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1089#1074#1086#1081#1089#1090#1074' '#1084#1072#1090#1077#1088#1080#1072#1083#1072
  ClientHeight = 208
  ClientWidth = 342
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object EditName: TEdit
    Left = 109
    Top = 16
    Width = 225
    Height = 21
    TabOrder = 0
  end
  object StaticText1: TStaticText
    Left = 8
    Top = 20
    Width = 52
    Height = 17
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077
    TabOrder = 1
  end
  object SpinEditCost: TSpinEdit
    Left = 109
    Top = 43
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 0
  end
  object StaticText2: TStaticText
    Left = 8
    Top = 48
    Width = 30
    Height = 17
    Caption = #1062#1077#1085#1072
    TabOrder = 3
  end
  object SpinEditDensity: TSpinEdit
    Left = 109
    Top = 71
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
  end
  object SpinEditFireResist: TSpinEdit
    Left = 109
    Top = 99
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object SpinEditStability: TSpinEdit
    Left = 109
    Top = 127
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
  end
  object StaticText3: TStaticText
    Left = 8
    Top = 76
    Width = 58
    Height = 17
    Caption = #1055#1083#1086#1090#1085#1086#1089#1090#1100
    TabOrder = 7
  end
  object StaticText4: TStaticText
    Left = 8
    Top = 104
    Width = 82
    Height = 17
    Caption = #1054#1075#1085#1077#1091#1087#1086#1088#1085#1086#1089#1090#1100
    TabOrder = 8
  end
  object StaticText5: TStaticText
    Left = 8
    Top = 132
    Width = 58
    Height = 17
    Caption = #1055#1088#1086#1095#1085#1086#1089#1090#1100
    TabOrder = 9
  end
  object ButtonOk: TButton
    Left = 152
    Top = 175
    Width = 89
    Height = 25
    Caption = #1054#1050
    TabOrder = 10
    OnClick = ButtonOkClick
  end
  object ButtonCancel: TButton
    Left = 247
    Top = 175
    Width = 89
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 11
    OnClick = ButtonCancelClick
  end
end
