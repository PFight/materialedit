unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Samples.Spin,
  Model;

type
  TForm2 = class(TForm)
    EditName: TEdit;
    StaticText1: TStaticText;
    SpinEditCost: TSpinEdit;
    StaticText2: TStaticText;
    SpinEditDensity: TSpinEdit;
    SpinEditFireResist: TSpinEdit;
    SpinEditStability: TSpinEdit;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    ButtonOk: TButton;
    ButtonCancel: TButton;
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    mEditingMaterial : TMaterial;
  public
    mModalResult: Integer;
    procedure setMaterial(mat: TMaterial);
    function getMaterial(): TMaterial;
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

{ TForm2 }

procedure TForm2.ButtonCancelClick(Sender: TObject);
begin
   mModalResult := mrCancel;
   Close();
end;

procedure TForm2.ButtonOkClick(Sender: TObject);
begin
   mEditingMaterial.name:=EditName.Text;
   mEditingMaterial.cost := SpinEditCost.Value;
   mEditingMaterial.density := SpinEditDensity.Value;
   mEditingMaterial.fire_resist := SpinEditFireResist.Value;
   mEditingMaterial.stability := SpinEditStability.Value;
   mModalResult := mrOk;
   Close();
end;



procedure TForm2.setMaterial(mat: TMaterial);
begin
    mEditingMaterial := mat;
    EditName.Text := mEditingMaterial.name;
    SpinEditCost.Value := Round(mEditingMaterial.cost);
    SpinEditDensity.Value := Round(mEditingMaterial.density);
    SpinEditFireResist.Value := Round(mEditingMaterial.fire_resist);
    SpinEditStability.Value := Round(mEditingMaterial.stability);

end;

function TForm2.getMaterial: TMaterial;
begin
  getMaterial:= mEditingMaterial;
end;

end.

